/* 
* hello.cpp
* The progamm prints some information about me.
* by MRWN
*/

#include <cstdio>

int main()
{
    // Prints out my name
    printf("Hello, my name is Matt.\n");
    // Prints out my hometown
    printf("I'm from Salzgitter, Niedersachsen.\n");
    // Prints out my hobby
    printf("I enjoy listening audio books.\n");
}